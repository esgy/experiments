import { importSchema } from 'graphql-import'
import { makeExecutableSchema } from 'graphql-tools'
import path from 'path'

// QUERIES
// -------------------------------------------------------------------------
// Accounts & Auth
import me from '../modules/accounts/queries/me'

// Search
import search from '../modules/search/queries/search'
import searches from '../modules/search/queries/searches'
import searchResults from '../modules/search/queries/searchResults'

// MUTATIONS
// -------------------------------------------------------------------------
// Accounts & Auth
import createAccount from '../modules/accounts/mutations/createAccount'
import loginWithPassword from '../modules/accounts/mutations/loginWithPassword'
// Searches
import searchCreate from '../modules/search/mutations/searchCreate'

// load GraphQL Type Definitions
const typeDefs = importSchema(path.join(__dirname, './schema.graphql'))

// The resolvers
const resolvers = {
  Query: {
    // Accounts
    me,

    // Search
    search,
    searches,
    searchResults
  },

  Mutation: {
    // Accounts
    createAccount,
    loginWithPassword,
    // Search
    searchCreate
  }
}

// Generate the schema object from your types definition.
export const schema = makeExecutableSchema({ typeDefs, resolvers })
