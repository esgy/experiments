// https://github.com/standard-things/esm
// fast, small, zero-dependency package is all you need to enable ES modules in Node 6+ today
require = require('@std/esm')(module)
module.exports = require('./server').default
