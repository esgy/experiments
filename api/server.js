import express from 'express'
import bodyParser from 'body-parser'
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express'
import mongoose from 'mongoose'
import helmet from 'helmet'
// import auth from 'http-auth'
import jwt from 'express-jwt'
import cors from 'cors'
import morgan from 'morgan'

// Load GraphQL API & Schemas and everything related to them (Resolvers & Types)
import { schema } from './schema/schema'

mongoose.Promise = Promise

// Initialize App
const app = express()

// logging
app.use(morgan('dev'))

// adding some security https://helmetjs.github.io/
app.use(helmet())

// TODO: activate this monitoring on production servers (exctract it to middleware helper)
// const basic = auth.basic({realm: 'Monitor Area'}, function (user, pass, callback) {
//   return callback(user === 'monitoring' && pass === '1234567890')
// })

// // MONITORING
// // Set '' to config path to avoid middleware serving the html page (path must be a string not equal to the wanted route)
// const statusMonitor = require('express-status-monitor')({ path: '' })
// app.use(statusMonitor.middleware) // use the "middleware only" property to manage websockets
// app.get('/status', auth.connect(basic), statusMonitor.pageRoute) // use the pageRoute property to serve the dashboard html page

// cross site resourse sharing configuration
app.use(cors())

app.use(bodyParser.json())

// The GraphQL endpoint
app.use('/graphql',
  jwt({
    secret: process.env.JWT_SECRET,
    credentialsRequired: false
  }),
  // request, response, graphQLParams
  graphqlExpress((req) => ({
    schema,
    context: {
      user: req.user
    }
  }))
)

if (process.env.NODE_ENV === 'development') {
  // GraphiQL, a visual editor for queries
  app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))
}

// Invalid JWT
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({error: 'INVALID TOKEN'})
  }
})

async function run () {
  // connect to MongoDB
  await mongoose.connect(process.env.MONGO_URL)

  // Start server
  app.listen(process.env.PORT, () => {
    return process.env.NODE_ENV === 'development' &&
      console.log(`Go to ${process.env.HOST}:${process.env.PORT}/graphiql to run queries!`)
  })
}

run().catch(error => console.error(error.stack))

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason)
  // Recommended: send the information to sentry.io
  // or whatever crash reporting service you use
})
