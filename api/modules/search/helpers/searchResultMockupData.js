export default [
  {
    'title': 'BMW 530 XD',
    'price': '8 287 EUR',
    'image': '',
    'description': '',
    'descriptionExtra': '',
    'year': '2007',
    'km': '257 tkm',
    'caroserie': 'sedan',
    'motor': 'diesel, 2 993 ccm, 170 kW',
    'linkToDetails': '/bmw-serie-5/sedan/diesel/bmw-530.html'
  },
  {
    'title': 'Audi A4 combi 1.9TDI 96kW, xeon',
    'price': '8 287 EUR',
    'image': '',
    'description': '',
    'descriptionExtra': '',
    'year': '2002',
    'km': '202 tkm',
    'caroserie': 'combi',
    'motor': 'diesel, 1 896 ccm, 96 kW',
    'linkToDetails': '/audi-a4/combi/diesel/audi-a4-combi-1-9tdi.html'
  },
  {
    'title': 'BMW 530d xDrive M-Perfomance',
    'price': '47 352 EUR',
    'image': '',
    'description': 'driver airbag, front passenger airbag deactivation, aut. climate control, CD player, Satellite Navigation, bluetooth, AUX, USB, ha...',
    'descriptionExtra': '',
    'year': '2016',
    'km': '30 tkm',
    'caroserie': 'sedan',
    'motor': 'diesel, 2 993 ccm, 190 kW',
    'linkToDetails': '/bmw-serie-5/sedan/diesel/bmw-530d-xdrive.html'
  }
]
