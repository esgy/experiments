import mongoose from 'mongoose'
import shortid from 'shortid'

const schema = new mongoose.Schema({
  _id: { type: String, default: shortid.generate, required: true },
  search: { type: String, required: true, ref: 'Search' }, // Search ID

  createdAt: { type: Date, default: Date.now, required: true },
  updatedAt: { type: Date, default: Date.now, required: true },

  title: { type: String, required: true, default: '' },
  price: { type: String, required: true, default: '' },
  image: { type: String, default: '' },
  description: { type: String, default: '' },
  descriptionExtra: { type: String, default: '' },
  year: { type: String, default: '' },
  km: { type: String, default: '' },
  caroserie: { type: String, default: '' }, // ex: sedan
  motor: { type: String, default: '' },
  linkToDetails: { type: String, default: '' }
})

export default mongoose.model('SearchResult', schema)
