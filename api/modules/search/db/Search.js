import mongoose from 'mongoose'
import shortid from 'shortid'

const fuelType = ['', 'diesel', 'petrol']
const currencyType = ['eur', 'usd']
const statusType = ['active', 'paused']

const schema = new mongoose.Schema({
  _id: { type: String, default: shortid.generate, required: true },
  account: { type: String, required: true, ref: 'Account' }, // ID

  createdAt: { type: Date, default: Date.now, required: true },
  updatedAt: { type: Date, default: Date.now, required: true },

  website: { type: String, required: true, lowercase: true, trim: true, default: '' },
  newIn: { type: Number, required: true, default: 1 }, // ex: 1 days
  searchInterval: { type: Number, required: true, default: 1 }, // ex: 1 minute

  make: { type: String, required: true, default: '' }, // ex: BMW
  model: { type: String, required: true, default: '' }, // ex: Serie-3

  fuel: { type: String, enum: fuelType },

  yearStart: { type: String },
  yearEnd: { type: String },

  priceStart: { type: String, default: '0' },
  priceEnd: { type: String },
  priceCurrency: { type: String, enum: currencyType, default: 'eur' },

  kmStart: { type: String, default: '0' },
  kmEnd: { type: String, default: '' },

  status: { type: String, enum: statusType, default: 'active' }
})

export default mongoose.model('Search', schema)
