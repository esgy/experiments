import Search from '../db/Search'

export default async function searchCreate (root, args, ctx) {
  let { user } = ctx
  if (!user) throw new Error('No user found')

  try {
    let searchToSave = args.search
    // attach current logged user ID
    searchToSave.account = user._id

    return await Search.create(searchToSave)
  } catch (err) {
    throw new Error(err)
  }
}
