import differenceInMinutes from 'date-fns/difference_in_minutes'

import Search from '../db/Search'
import SearchResult from '../db/SearchResult'

// TODO: load here result mockups
import searchResultMockupData from '../helpers/searchResultMockupData'

// TODO: check the `status` to see if the search is active or not
// TODO: add a `FORCE` flag to always scrape

export default async function searchResults (root, args, ctx) {
  let { user } = ctx
  if (!user) throw new Error('No user found')

  // get the ID of the saved search to load results from
  let { searchId, forceScrape = false } = args

  // get Search info and check account-search ownership
  let search = await Search.findOne({ _id: searchId })
  if (search.account !== user._id) throw new Error('You do not own this search!')

  // check if the chache needs refreshing, by checking the last updated time
  // and the recheck period for this search
  let minutesSinceLastUpdate = differenceInMinutes(new Date(), search.updatedAt)
  // console.log('minutesSinceLastUpdate>>> ', minutesSinceLastUpdate)

  let searchExpired = minutesSinceLastUpdate > search.searchInterval

  // check if there are any results for this search
  let existingResults = await SearchResult.find({ search: searchId })

  // Scrape if forced, expired or no existing results
  if (forceScrape || searchExpired || (existingResults.length === 0)) {
    console.log('>>> Scraping... <<<<', searchResultMockupData)

    // TODO: use here the mockup data

    // Let's attach the current search ID to all of the results
    let searchResult = searchResultMockupData.map(searchResult => {
      return { ...searchResult, search: search._id }
    })

    // save the results to the database (for caching purposes)   .insertMany
    try {
      // 1st remove the existing results to avoid duplicates
      await SearchResult.deleteMany({ search: search._id })
      // now insert what was scrapped
      await SearchResult.insertMany(searchResult)

      // update updatedAt for this Search
      await Search.findOneAndUpdate({ _id: searchId }, { $set: { updatedAt: new Date() } })

      // return the NEW results
      return await SearchResult.find({ search: searchId })
    } catch (e) {
      return new Error('ERROR INSERT MANY', e)
    }
  }

  try {
    // Results exist or search not expired, so return the existing results
    return SearchResult.find({ search: searchId })
  } catch (e) {
    return new Error('FIND>', e)
  }
}
