import Search from '../db/Search'

export default async function searches (root, args, ctx) {
  let { user } = ctx
  if (!user) throw new Error('No user found')

  // get the ID of the saved search to load results from
  let { searchId } = args

  try {
    // get Search info and check account-search ownership
    let search = await Search.findOne({ _id: searchId }).populate('account')

    // check for owner
    if (search.account._id !== user._id) throw new Error('You do not own this search!')

    return search
  } catch (err) {
    throw new Error(err)
  }
}
