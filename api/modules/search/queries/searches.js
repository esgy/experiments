import Search from '../db/Search'

export default async function searches (root, args, ctx) {
  let { user } = ctx
  if (!user) throw new Error('No user found')

  try {
    return await Search.find({})
  } catch (err) {
    throw new Error(err)
  }
}
