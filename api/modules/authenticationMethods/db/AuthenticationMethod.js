/**
 * We create authentication methods in a separate collection
 * to avoid loading them in Profile by accident
 */
import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import shortid from 'shortid'

const HASH_ROUNDS = 10
const AUTH_METHOD_TYPES = ['OAUTH', 'PASSWORD']

const authenticationMethodSchema = new mongoose.Schema({
  _id: { type: String, default: shortid.generate },
  type: { type: String, enum: AUTH_METHOD_TYPES },
  accountId: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: function () { return this.type === 'PASSWORD' }
  },
  accessToken: {
    type: String,
    required: function () { return this.type === 'OAUTH' }
  }
})

// hash password using bcrypt before saving
authenticationMethodSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    return bcrypt.hash(this.password, HASH_ROUNDS, (err, hash) => {
      if (err) {
        return next(err)
      }
      this.password = hash
      next()
    })
  }
  next()
})

// TODO: verifiy password helper

export default mongoose.model('AuthenticationMethod', authenticationMethodSchema)
