import Account from '../db/Account'

export default async function accounts (_, args, ctx) {
  let { user } = ctx
  if (!user) throw new Error('No user found')

  // args
  try {
    return await Account.find({})
  } catch (err) {
    console.error('ERR', err)
    throw new Error(err)
  }
}
