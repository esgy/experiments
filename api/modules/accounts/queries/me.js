import Account from '../db/Account'

export default async function me (_, args, ctx) {
  let { user } = ctx
  if (!user) return false

  try {
    return await Account.findOne({_id: user._id})
  } catch (err) {
    throw new Error(err)
  }
}
