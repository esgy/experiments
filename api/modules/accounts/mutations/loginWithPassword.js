import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import Account from '../db/Account'
import AuthenticationMethod from '../../authenticationMethods/db/AuthenticationMethod'

export default async function loginWithPassword (_, args, ctx) {
  // ctx.user.username

  let { username, password } = args

  // get current user based on username
  let account = await Account.findOne({
    username: username
  }, 'username')

  if (!account) {
    throw new Error('No account found')
  }

  // get the reference AthenticationMethod
  let authMethod = await AuthenticationMethod.findOne({
    accountId: account._id,
    type: 'PASSWORD'
  })

  // test password
  let passwordIsValid = await bcrypt.compare(password, authMethod.password)

  // generate JWT token
  let tokenOptions = {
    expiresIn: process.env.JWT_EXPIRATION_TIME
  }
  let token = jwt.sign(
    { _id: account.id, username: account.username },
    process.env.JWT_SECRET,
    tokenOptions
  )

  // attach token to the Account data
  account.token = {
    jwt: token,
    error: null
  }

  if (passwordIsValid) {
    return account
  } else {
    throw new Error('Wrong user or password')
  }
}
