import jwt from 'jsonwebtoken'
import Account from '../db/Account'
import AuthenticationMethod from '../../authenticationMethods/db/AuthenticationMethod'

export default async function createAccount (root, args, other) {
  let { username, password } = args

  // check if user already exists
  let existingUser = await Account.findOne({ username: username })
  if (existingUser) {
    throw new Error('Account already exists')
  }

  try {
    // create account
    let account = await Account.create({
      username: username,
      emails: [
        {
          address: username,
          verified: false
        }
      ]
    })

    // create auth method
    await AuthenticationMethod.create({
      accountId: account._id,
      type: 'PASSWORD',
      password: password
    })

    // generate JWT token
    let tokenOptions = {
      expiresIn: process.env.JWT_EXPIRATION_TIME
    }
    let token = jwt.sign(
      { _id: account.id, username: account.username },
      process.env.JWT_SECRET,
      tokenOptions
    )

    // attach token to the Account data
    account.token = {
      jwt: token,
      error: null
    }

    // TODO: send email for validation

    return account
  } catch (err) {
    throw new Error(err)
  }
}
