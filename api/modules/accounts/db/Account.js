import mongoose from 'mongoose'
import shortid from 'shortid'

const accountRoles = ['user', 'admin']

const schema = new mongoose.Schema({
  _id: { type: String, default: shortid.generate },
  username: { type: String, unique: true, required: true, lowercase: true, trim: true },
  createdAt: { type: Date, default: Date.now, required: true },
  updatedAt: { type: Date, default: Date.now, required: true },
  emails: [{
    _id: false,
    address: { type: String, lowercase: true, trim: true, unique: true },
    verified: { type: Boolean, default: false }
  }],
  profile: {
    fullname: String,
    address: String,
    phone: String
  },
  roles: { type: Array, default: ['user'], enum: accountRoles }
})

export default mongoose.model('Account', schema)
