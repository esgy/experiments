// https://medium.com/@Abazhenov/using-async-await-in-express-with-node-8-b8af872c0016
// TODO: actually use it.. in the resolvers
export default fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next)
}
