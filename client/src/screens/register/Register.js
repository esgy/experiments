import React, { Component } from 'react'
import {
  Link
} from 'react-router-dom'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

import logo from '../../assets/images/logo-icon.png'
import './Register.css'

class Register extends Component {

  state = {
    isLoggingIn: false,
    email: '',
    password: ''
  }

  componentDidMount() {
    if (window.localStorage.getItem('token')) {
      return this.props.history.push('/my-searches')
    }
  }

  onRegister = async (e) => {
    e.preventDefault()

    this.setState({
      isLoggingIn: true
    })

    try {
      let { data: { createAccount } } = await this.props.createAccount(
        {
          variables: {
            username: this.state.email,
            password: this.state.password
          }
        }
      )

      if (!createAccount) {
        this.setState({
          isLoggingIn: false
        })
        window.alert('wrong user pass')
      }

      // Save JWT token to local storage
      await window.localStorage.setItem('token', createAccount.token.jwt)

      // redirect
      this.props.history.push('/my-searches')

    } catch (err) {
      console.error('Querry Error >>', err);
      window.alert(err.message)
      this.setState({
        isLoggingIn: false
      })
    }

  }

  render () {
    return (
      <form className='form-register'>
        <div className='text-center mb-4'>
          <img className='mb-3 img-fluid' src={logo} alt='' width='50%' />
          <br/>
          <br/>
          <h1 className='h3 mb-3 font-weight-normal'>Register</h1>
        </div>

        <div className='form-label-group'>
          <input
            type='email'
            id='inputEmail'
            className='form-control'
            placeholder='Email address'
            required
            value={this.state.email}
            onChange={(e) => this.setState({ email: e.target.value })}
          />
          <label htmlFor='inputEmail'>Email address</label>
        </div>

        <div className='form-label-group'>
          <input
            type='password'
            id='inputPassword'
            className='form-control'
            placeholder='Password'
            required
            value={this.state.password}
            onChange={(e) => this.setState({ password: e.target.value })}
          />
          <label htmlFor='inputPassword'>Password</label>
        </div>

        {this.state.isLoggingIn
          ? <button
              className='btn btn-lg btn-primary btn-block' disabled
              type='button'
            >Signing in...</button>
          : <button
              className='btn btn-lg btn-primary btn-block'
              type='button'
              onClick={this.onRegister}
            >Create account</button>
        }

        <hr/>
        <p className='text-center'>
          <Link to={'/'}>
            Login
          </Link>
        </p>

      </form>
    )
  }
}

const CREATE_ACCOUNT_MUTATION = gql`
  mutation createAccount($username: String!, $password: String!) {
    createAccount(username: $username, password: $password) {
      _id
      username
      token {
        jwt
        error
      }
    }
  }
`

export default graphql(CREATE_ACCOUNT_MUTATION, { name: 'createAccount' })(Register)

