import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

import logo from '../../assets/images/logo-icon.png'
import './Login.css'

class Login extends Component {

  state = {
    isLoggingIn: false,
    email: '',
    password: ''
  }

  componentDidMount() {
    if (window.localStorage.getItem('token')) {
      return this.props.history.push('/my-searches')
    }
  }

  onLogin = async (e) => {
    e.preventDefault()

    this.setState({
      isLoggingIn: true
    })

    try {
      let { data: { loginWithPassword } } = await this.props.loginWithPassword(
        {
          variables: {
            username: this.state.email,
            password: this.state.password
          }
        }
      )

      // Save JWT token to local storage
      await window.localStorage.setItem('token', loginWithPassword.token.jwt)

      // redirect
      this.props.history.push('/my-searches')

    } catch (err) {
      window.alert(err)
      console.error('Querry Error >>>', err)

      this.setState({
        isLoggingIn: false
      })
    }

  }

  render () {
    return (
      <form className='form-signin'>
        <div className='text-center mb-4'>
          <img className='mb-3 img-fluid' src={logo} alt='' width='50%' />
          <br/>
          <br/>
          <h1 className='h3 mb-3 font-weight-normal'>Login</h1>
        </div>

        <div className='form-label-group'>
          <input
            type='email'
            id='inputEmail'
            className='form-control'
            placeholder='Email address'
            required
            value={this.state.email}
            onChange={(e) => this.setState({ email: e.target.value })}
          />
          <label htmlFor='inputEmail'>Email address</label>
        </div>

        <div className='form-label-group'>
          <input
            type='password'
            id='inputPassword'
            className='form-control'
            placeholder='Password'
            required
            value={this.state.password}
            onChange={(e) => this.setState({ password: e.target.value })}
          />
          <label htmlFor='inputPassword'>Password</label>
        </div>

        {this.state.isLoggingIn
          ? <button
              className='btn btn-lg btn-primary btn-block' disabled
              type='button'
            >Signing in...</button>
          : <button
              className='btn btn-lg btn-primary btn-block'
              type='button'
              onClick={this.onLogin}
            >Login</button>
        }

        <hr/>
        <p className='text-center'>
          <Link to={'/register'}>
            Register new account
          </Link>
        </p>

      </form>
    )
  }
}


const LOGIN_WITH_PASSWORD_MUTATION = gql`
  mutation loginWithPassword($username: String!, $password: String!) {
    loginWithPassword(username: $username, password: $password) {
      _id
      username
      token {
        jwt
        error
      }
    }
  }
`

export default graphql(LOGIN_WITH_PASSWORD_MUTATION, { name: 'loginWithPassword' })(Login)
