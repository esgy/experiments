import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import { graphql, compose } from 'react-apollo'
import { formatDistance } from 'date-fns/esm'

import Navigation from '../navigation/Navigation'
import MySearchResultItem from './MySearchResultItem'

class MySearchResults extends Component {
  render () {
    console.log('MY SERACH RESULTS DATA PROPS', this.props)
    let { search, searchResults, match: { params } } = this.props
    return (
      <Fragment>
        <Navigation />
        <main role='main' className='container pt-5'>
          <h4 className='pt-3'>
            <Link to={`/my-searches`}><i className='material-icons md-24' title='My Searches'>chevron_left</i></Link>
            {this.props.loading
              ? <span>Loading...</span>
              : <Fragment>
                {search.make} {search.model} {search.fuel} <br />
                <small>
                  Eur: {search.priceStart}-{search.priceEnd} / KM: {search.kmStart}-{search.kmEnd}
                </small>
              </Fragment>
            }
          </h4>

          {this.props.loading
            ? null
            : <Fragment>
              <small>Updated {formatDistance(new Date(), new Date(search.updatedAt))}</small>

              {searchResults
                ? searchResults.map(searchResultItem => {
                  return <MySearchResultItem
                    key={searchResultItem._id}
                    searchId={params.id}
                    data={searchResultItem}
                  />
                })
                : <div>No Results found!</div>}
            </Fragment>
          }
        </main>
      </Fragment>
    )
  }
}

// TODO: query for the search details
const MY_SEARCH_QUERY = gql`
  query search($searchId: ID!) {
    search(searchId: $searchId) {
      _id
      account {
        _id
        username
      }
      createdAt
      updatedAt

      newIn
      searchInterval

      make
      model
      fuel

      yearStart
      yearEnd

      priceStart
      priceEnd
      priceCurrency

      kmStart
      kmEnd
    }
  }
`

let SEARCH_RESULTS_QUERY = gql`
  query searchResults($searchId: ID!) {
    searchResults(searchId: $searchId) {
      _id
      search {
        _id
        model
      }
      createdAt
      updatedAt
      title
      price
      image
      description
      descriptionExtra
      year
      km
      caroserie
      motor
    }
  }
`

export default compose(
  graphql(MY_SEARCH_QUERY, {
    options: ({ match }) => {
      return {
        fetchPolicy: 'network-only',
        variables: {
          searchId: match.params.id
        }
      }
    },
    props: ({ data: { loading, search } }) => {
      return {
        loading: loading,
        search: search
      }
    }
  }),
  graphql(SEARCH_RESULTS_QUERY, {
    options: ({ match }) => {
      return {
        fetchPolicy: 'network-only',
        variables: {
          searchId: match.params.id
        }
      }
    },
    props: ({ data: { loading, searchResults } }) => {
      return {
        loading: loading,
        searchResults: searchResults
      }
    }
  })
)(MySearchResults)
