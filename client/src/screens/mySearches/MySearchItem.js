import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formatDistance } from 'date-fns/esm'

// TODO: add component prop types

class MySearchItem extends Component {
  render () {
    console.log('DATA', this.props.data)
    let {
      _id,
      make,
      model,
      fuel,
      updatedAt,
      newIn,
      searchInterval,

      yearStart,
      yearEnd,

      priceStart,
      priceEnd,
      priceCurrency,

      kmStart,
      kmEnd
    } = this.props.data
    return (
      <Link to={`/my-searches/${_id}`}>
        <div className='my-3 p-3 bg-white rounded box-shadow'>
          <div className='media pt-1' title={_id}>
            <div className='media-body pb-1 mb-0 lh-125 border-gray'>
              <div className='d-flex justify-content-between'>
                <b className='text-gray-dark'>{make} {model}</b>
                <span className='text-muted'>{fuel || 'all'}</span>
                <span className='badge badge-secondary'>
                  {newIn} day
                </span>
              </div>
              <div className='d-flex text-muted'>
                <span>{priceStart || 0} - {priceEnd || 0} {priceCurrency} &nbsp;</span>
                {yearStart || yearEnd ? <span>{yearStart} - {yearEnd}</span> : null}
                {kmStart || kmEnd ? <span>{kmStart || 0}Km - {kmEnd || 0}Km</span> : null}
              </div>
              <div className='d-flex justify-content-between text-muted'>
                <small>
                  Updated {formatDistance(new Date(), new Date(updatedAt))}
                </small>
                <small className='text-muted'>
                  Every {searchInterval} min
                </small>
              </div>
            </div>
          </div>
        </div>
      </Link>
    )
  }
}

export default MySearchItem
