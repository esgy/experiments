import React, { Component } from 'react'
import { Link } from 'react-router-dom'
// import { formatDistance } from 'date-fns/esm'

// TODO: add component prop types

class MySearchResultItem extends Component {
  render () {
    console.log('MySearchResultItem', this.props.data)
    let {
      _id,
      // createdAt,
      // updatedAt,
      title,
      price,
      image,
      description,
      // descriptionExtra,
      year,
      km,
      caroserie,
      motor
    } = this.props.data
    let { searchId } = this.props
    return (
      <Link to={`/my-searches/${searchId}/details/${_id}`}>
        <div className='my-3 p-3 bg-white rounded box-shadow'>
          <div className='media pt-1' title={title}>
            <div className='d-flex media-body pb-1 mb-0 lh-125 border-gray text-muted'>
              {image ? <img src={image} alt='' className='img-thumbnail rounded w-25 h-25' /> : null}
              <div>
                <h6 className='text-gray-dark'>{title}</h6>
                <h6>{price}, {year}</h6>
                <div className='d-flex justify-content-between'>
                  {km}, {caroserie}, {motor}
                </div>
                <div className='d-flex justify-content-between'>
                  <p className='small'>
                    {description}
                  </p>
                </div>
                {/* <div className='d-flex justify-content-between'>
                  {formatDistance(new Date(), new Date(createdAt))},
                  {formatDistance(new Date(), new Date(updatedAt))}
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </Link>
    )
  }
}

export default MySearchResultItem
