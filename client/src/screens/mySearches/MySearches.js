import React, { Component, Fragment } from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

import Navigation from '../navigation/Navigation'

import './MySearches.css'
import MySearchItem from './MySearchItem'

class MySearches extends Component {
  render () {
    return (
      <Fragment>
        <Navigation />

        <main role='main' className='container pt-5'>
          <h4 className='pt-3'>My Searches</h4>
          {this.props.loading
            ? <div>Loading...</div>
            : <Fragment>
              {this.props.searches
              ? this.props.searches.map(searchItem => {
                return <MySearchItem key={searchItem._id} data={searchItem} />
              })
              : <div>No Results found!</div>}
            </Fragment>
          }
        </main>
      </Fragment>
    )
  }
}

const MY_SEARCHES_QUERY = gql`
  query searches {
    searches {
      _id
      account {
        _id
        username
      }
      createdAt
      updatedAt

      newIn
      searchInterval

      make
      model
      fuel

      yearStart
      yearEnd

      priceStart
      priceEnd
      priceCurrency

      kmStart
      kmEnd
    }
  }
`

export default graphql(MY_SEARCHES_QUERY, {
  options: { fetchPolicy: 'network-only' },
  props: ({ data: { loading, searches } }) => ({
    loading,
    searches
  })
})(MySearches)
