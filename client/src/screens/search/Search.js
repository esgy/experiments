import React, { Component, Fragment } from 'react'
// import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

import Navigation from '../navigation/Navigation'

// TODO: load list of Makes & Models and populate the form

class Search extends Component {
  state = {
    isCreating: false,
    form: {
      website: 'mobile.de',
      newIn: 1, // days
      searchInterval: 1, // minutes
      make: '',
      model: '',
      fuel: '',
      yearStart: 1900,
      yearEnd: '',
      priceStart: 0,
      priceEnd: '',
      priceCurrency: 'eur',
      kmStart: '',
      kmEnd: ''
    }
  }

  onSubmitForm = async (e) => {
    e.preventDefault()

    this.setState({
      isCreating: true
    })

    // get form data
    console.log('FORM DATA', this.state.form)
    // TODO: redirect to the details view of the saved search (where we load the actual results of the search)


    try {
      let { data } = await this.props.searchCreate({
        variables: {
          search: this.state.form
        }
      })

      console.log('searchCreate RESULT', data)

      // redirect
      this.props.history.push('/my-searches')
    } catch (err) {
      console.error('Querry Error >>>', err)
      this.setState({
        isCreating: false
      })
    }
  }

  render () {
    return (
      <Fragment>
        <Navigation />

        <form className='container pt-5'>

          <h3 className='pt-2 pb-4'>New Search</h3>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Search:</label>
            <div className="col-8">
              <select
                className='form-control'
                value={this.state.form.website}
                onChange={(e) => this.setState(Object.assign(this.state.form, { website: e.target.value }))}
              >
                <option value="mobile.de">mobile.de</option>
              </select>
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>New In</label>
            <div className="col-8">
              <select
                className='form-control'
                value={this.state.form.newIn}
                onChange={(e) => this.setState(Object.assign(this.state.form, { newIn: e.target.value }))}
              >
                <option value="1">1 day</option>
                <option value="2">2 days</option>
                <option value="3">3 days</option>
                <option value="5">5 days</option>
                <option value="7">7 days</option>
                <option value="10">10 days</option>
                <option value="20">20 days</option>
                <option value="35">35 days</option>
              </select>
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Every</label>
            <div className="col-8">
              <select
                className='form-control'
                value={this.state.form.searchInterval}
                onChange={(e) => this.setState(Object.assign(this.state.form, { searchInterval: e.target.value }))}
              >
                <option value="1">1 min</option>
                <option value="3">3 min</option>
                <option value="5">5 min</option>
                <option value="10">10 min</option>
                <option value="15">15 min</option>
                <option value="30">30 min</option>
                <option value="60">1 hour</option>
                <option value="120">2 hours</option>
                <option value="180">3 hours</option>
                <option value="360">6 hours</option>
                <option value="720">12 hours</option>
                <option value="1440">1 day</option>
                <option value="2880">2 days</option>
                <option value="4320">3 days</option>
                <option value="7200">5 days</option>
              </select>
            </div>
          </div>

          <hr/>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Make *</label>
            <div className="col-8">
              <select
                className='form-control'
                value={this.state.form.make || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { make: e.target.value }))}
              >
                <option value="">-</option>
                <option value="audi">Audi</option>
                <option value="bmw">BMW</option>
              </select>
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Model *</label>
            <div className="col-8">
              <select
                className='form-control'
                value={this.state.form.model || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { model: e.target.value }))}
              >
                <option value="">-</option>
                <option value="a4">A4</option>
                <option value="a6">A6</option>
                <option value="serie-1">Serie 1</option>
                <option value="serie-3">Serie 3</option>
                <option value="serie-5">Serie 5</option>
                <option value="m2">M2</option>
              </select>
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Fuel</label>
            <div className="col-8">
              <select
                className='form-control'
                value={this.state.form.fuel || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { fuel: e.target.value }))}
              >
                <option value="">Any</option>
                <option value="diesel">Diesel</option>
                <option value="petrol">Petrol</option>
              </select>
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Years</label>
            <div className="col-8 d-flex">
              <input
                type="number"
                className='form-control'
                placeholder="Start"
                value={this.state.form.yearStart || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { yearStart: e.target.value }))}
              />
              &nbsp;
              <input
                type="number"
                className='form-control'
                placeholder="End"
                value={this.state.form.yearEnd || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { yearEnd: e.target.value }))}
              />
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>Price (Eur)</label>
            <div className="col-8 d-flex">
              <input
                type="number"
                className='form-control'
                placeholder="Start"
                value={this.state.form.priceStart}
                onChange={(e) => this.setState(Object.assign(this.state.form, { priceStart: e.target.value }))}
              />
              &nbsp;
              <input
                type="number"
                className='form-control'
                placeholder="End"
                value={this.state.form.priceEnd || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { priceEnd: e.target.value }))}
              />
            </div>
          </div>

          <div className='form-group row'>
            <label className='col-form-label col-4'>KM</label>
            <div className="col-8 d-flex">
              <input
                type="number"
                className='form-control'
                placeholder="Start"
                value={this.state.form.kmStart || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { kmStart: e.target.value }))}
              />
              &nbsp;
              <input
                type="number"
                className='form-control'
                placeholder="End"
                value={this.state.form.kmEnd || ''}
                onChange={(e) => this.setState(Object.assign(this.state.form, { kmEnd: e.target.value }))}
              />
            </div>
          </div>

          <hr/>

          <div className='d-flex justify-content-center'>
            {this.state.isCreating
              ? <button
                  className='btn btn-lg btn-primary btn-block' disabled
                  type='button'
                >Creating new search...</button>
              : <button
                  className='btn btn-primary'
                  disabled={!this.state.form.make || !this.state.form.model}
                  onClick={this.onSubmitForm}
                >Create New Search</button>
            }
          </div>
        </form>
      </Fragment>
    )
  }
}

const SEARCH_CREATE_MUTATION = gql`
  mutation searchCreate($search: SearchInput!) {
    searchCreate(search: $search) {
      _id
      account {
        _id
        username
      }
      createdAt
      updatedAt

      website
      newIn
      searchInterval

      make
      model
      fuel

      yearStart
      yearEnd

      priceStart
      priceEnd
      priceCurrency

      kmStart
      kmEnd
    }
  }
`

export default graphql(SEARCH_CREATE_MUTATION, { name: 'searchCreate' })(Search)

