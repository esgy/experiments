import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import logo from '../../assets/images/logo-icon.png'

import './Navigation.css'

class Navigation extends Component {
  render () {
    return (
      <div className='nav-scroller bg-white box-shadow position-fixed w-100'>
        <nav className='nav nav-underline'>
          <NavLink className='nav-link' to={'/my-searches'}>
            <i className='material-icons md-24' title='My Searches'>book</i>
          </NavLink>
          <NavLink className='nav-link' to={'/search'}>
            <i className='material-icons md-24' title='Search'>search</i>
          </NavLink>
          <div className='nav-link ml-auto mr-auto pt-1'><img src={logo} width='40' alt='' /></div>
          <NavLink className='nav-link ml-auto' to={'/profile'}>
            <i className='material-icons md-24' title='Profile'>face</i>
          </NavLink>
        </nav>
      </div>
    )
  }
}

export default Navigation
