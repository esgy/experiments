import React, { Component, Fragment } from 'react'
// import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import { graphql, withApollo } from 'react-apollo'

import logo from '../../assets/images/logo-icon.png'

import Navigation from '../navigation/Navigation'

class Profile extends Component {
  onLogout = async (e) => {
    e.preventDefault()

    // 1. remove the token (the order matters)
    await window.localStorage.removeItem('token')

    // 2. clean Apollo browser cache
    await this.props.client.resetStore()

    // 3. redirect to home (maybe this should be .replace ??)
    await this.props.history.push('/')
  }

  render () {
    let { me } = this.props
    if (!me) return <div>User not found.</div>;
    return (
      <Fragment>
        <Navigation />

        <main role='main' className='container pt-5'>

          <div className='text-center mt-4'>
            <img src={logo} width='128' alt='' />
          </div>

          <br />

          {this.props.loading
            ? <p>Loading..</p>
            : <Fragment>
              <p>Email: {me && me.username}</p>
              <p><a href="" onClick={this.onLogout}>Logout</a></p>
            </Fragment>}

        </main>
      </Fragment>
    )
  }
}

const ME_QUERY = gql`
  query me {
    me {
      _id
      username
      roles
      updatedAt
      createdAt
      emails {
        address
        verified
      }
    }
  }
`

export default withApollo(graphql(ME_QUERY, {
  options: { fetchPolicy: 'network-only' },
  props: ({ data: { loading, me } }) => ({
    loading,
    me,
  }),
})(Profile))
