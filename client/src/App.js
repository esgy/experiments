import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from 'react-apollo'
import { onError } from 'apollo-link-error'

// Styles
import 'bootstrap/dist/css/bootstrap.css'
import 'material-design-icons/iconfont/material-icons.css'
import './App.css'

// Helpers
import PrivateRoute from './helpers/PrivateRoute'

// Screens
import Login from './screens/login/Login'
import Register from './screens/register/Register'
import MySearches from './screens/mySearches/MySearches'
import MySearchResults from './screens/mySearches/MySearchResults'
import Search from './screens/search/Search'
import Profile from './screens/profile/Profile'

// TODO: export this to a .env file
const GQLHOST = 'http://localhost:3001/graphql'

const httpLink = createHttpLink({
  uri: GQLHOST
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      console.error(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    )
  }
  if (networkError) {
    console.warn(`[Network error]:`, networkError.message)

    // TODO: redirect to login or Refresh page?, we don't have access to router here
    // window.location.href = '/login'
    if (networkError.message.includes('401')) {
      return window.localStorage.removeItem('token')
    }
  }
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = window.localStorage.getItem('token')
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const link = ApolloLink.from([
  errorLink,
  authLink,
  httpLink
])

const client = new ApolloClient({
  connectToDevTools: true,  // TODO: read node ENV to disable in production
  link: link,
  cache: new InMemoryCache(),
  queryDeduplication: true
  // dataIdFromObject: o => o.id
})

class App extends Component {
  render () {
    return (
      <ApolloProvider client={client}>
        <Router>
          <Switch>
            <Route exact path='/' component={Login} />
            <Route exact path='/register' component={Register} />
            <PrivateRoute exact path='/my-searches' component={MySearches} />
            <PrivateRoute exact path='/my-searches/:id' component={MySearchResults} />
            <PrivateRoute exact path='/search' component={Search} />
            <PrivateRoute exact path='/profile' component={Profile} />
          </Switch>
        </Router>
      </ApolloProvider>
    )
  }
}

export default App
