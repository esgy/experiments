# NOTICE: This is a DEMO setup. Client app best viewed in Device mode for iPhone 6 Plus.


# Experiment: Apollo Graphql server, ReactJS + React Router 4 Client

### Objective
Use this repo to test Apollo GraphQL new features and possibly use it as starting boilerplate for new projects.
Extracted from an existing, work in progress, project.

## Server API

`cd api`
`npm i`
`npm run dev`

## Client

`cd client`
`npm i`
`npm run dev`


## TODOs

- [ ] Continue researching advanced JWT security. Need to find safe way to refresh tokens.
- [ ] Server side rendering using Apollo and React Router 4 (https://www.apollographql.com/docs/react/recipes/server-side-rendering.html)
- [ ] Implement GraphQL subscriptions with Websockets (https://www.apollographql.com/docs/graphql-subscriptions/)
- [ ] Experiment with Schema Stitching (https://www.apollographql.com/docs/graphql-tools/schema-stitching.html)
- [ ] Client side Code Splitting with React Router 4
